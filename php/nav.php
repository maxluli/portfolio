<nav id="navbar_top" class="navbar navbar-expand-lg fixed-top" style="">
  <div class="container-fluid">
    <a class="" href="#">Maxluli</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link  " aria-current="page" href="#">About me</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Projects
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item " href="#">Web Projects</a></li>
            <li><a class="dropdown-item " href="#">C# Projects</a></li>
            <li><a class="dropdown-item " href="#">Electronics Projects</a></li>
            <li>
              <hr class="dropdown-divider">
            </li>
            <li><a class="dropdown-item " href="#">All Projects</a></li>
          </ul>
        </li>
        <li class="nav-item">
          <a class="nav-link active " aria-current="page" href="#">Hobbys</a>
        </li>
      </ul>
    </div>
  </div>
</nav>


